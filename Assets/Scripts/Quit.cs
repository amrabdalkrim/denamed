﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class Quit : MonoBehaviour
{
    public void EndGame() => Application.Quit();

}
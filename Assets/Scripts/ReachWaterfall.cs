﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReachWaterfall : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene(3);
    }

    private void OnCollisionEnter(Collision collision)
    {
        SceneManager.LoadScene(3);
    }
}
